# SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

FROM nicolasconstant/sengi:release-1.7.1

COPY custom/privacy.html /app/assets/docs/privacy.html
