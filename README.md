<!--
SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/sengi/00_README)
[![REUSE status](https://api.reuse.software/badge/git.fsfe.org/fsfe-system-hackers/sengi)](https://api.reuse.software/info/git.fsfe.org/fsfe-system-hackers/sengi)

# Sengi

Sengi is a Mastodon and Pleroma desktop focused client. It takes
inspiration from the old Tweetdeck client, the new Tweetdeck webapp and
Mastodon UI. It is Free Software under AGPL-3.0. See the [upstream
source](https://github.com/NicolasConstant/sengi) for more information.

This instance is deployed as a web app, so you can use it freely.
Registration is not required.

All authentication data is stored inside your browser, so we cannot get
access to your account.

## Troubleshooting

To avoid having to re-authenticate your account regularly, e.g. after a
restart of your browser, do not delete your browser data for
sengi.fsfe.org.
